<?php

/**
 * 361GRAD Element Teaser Simple
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

$GLOBALS['TL_LANG']['CTE']['dse_elements']     = 'DSE-Elemente';
$GLOBALS['TL_LANG']['CTE']['dse_teasertags'] = ['Teaser mit Tags', 'Teaser Block.'];

$GLOBALS['TL_LANG']['tl_content']['dse_text']      = ['Text', ''];

$GLOBALS['TL_LANG']['tl_content']['dse_image']     = ['Bild', ''];
$GLOBALS['TL_LANG']['tl_content']['dse_imageSize'] = ['Bild Größe', ''];
