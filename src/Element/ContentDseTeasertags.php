<?php

/**
 * 361GRAD Element Teaser with tags
 *
 * @package   dse-elements-bundle
 * @author    Chris Kirchmaier <chris@361.de>
 * @copyright 2016 361GRAD
 * @license   http://www.361.de proprietary
 */

namespace Dse\ElementsBundle\ElementTeasertags\Element;

use Contao\BackendTemplate;
use Contao\ContentElement;
use Contao\File;
use Contao\FilesModel;
use Contao\FrontendTemplate;
use Contao\StringUtil;
use Patchwork\Utf8;

/**
 * Class ContentDseTeasertags
 *
 * @package Dse\ElementsBundle\Elements
 */
class ContentDseTeasertags extends ContentElement
{
    /**
     * Template name.
     *
     * @var string
     */
    protected $strTemplate = 'ce_dse_teasertags';


    /**
     * Display a wildcard in the back end.
     *
     * @return string
     */
    public function generate()
    {
        return parent::generate();
    }


    /**
     * Generate the module
     *
     * @return void
     */
    protected function compile()
    {
        // Build subheadline like Contao headline
        $arrSubheadline              = StringUtil::deserialize($this->dse_subheadline);
        $this->Template->subheadline = is_array($arrSubheadline) ? $arrSubheadline['value'] : $arrSubheadline;
        $this->Template->shl         = is_array($arrSubheadline) ? $arrSubheadline['unit'] : 'h2';

        // Set up Images in template
        $self = $this;

        $this->Template->getImageObject = function () use ($self) {
            return call_user_func_array(array($self, 'getImageObject'), func_get_args());
        };
    }


    /**
     * Get an image object from uuid
     *
     * @param       $uuid
     * @param null  $size
     * @param null  $maxSize
     * @param null  $lightboxId
     * @param array $item
     *
     * @return \FrontendTemplate|object
     */
    public function getImageObject($uuid, $size = null, $maxSize = null, $lightboxId = null, $item = array())
    {
        global $objPage;

        if (!$uuid) {
            return null;
        }

        $image = FilesModel::findByUuid($uuid);

        if (!$image) {
            return null;
        }

        try {
            $file = new File($image->path, true);
            if (!$file->exists()) {
                return null;
            }
        } catch (\Exception $e) {
            return null;
        }

        $imageMeta = $this->getMetaData($image->meta, $objPage->language);

        if (is_string($size) && trim($size)) {
            $size = deserialize($size);
        }
        if (!is_array($size)) {
            $size = array();
        }
        $size[0] = isset($size[0]) ? $size[0] : 0;
        $size[1] = isset($size[1]) ? $size[1] : 0;
        $size[2] = isset($size[2]) ? $size[2] : 'crop';

        $image = array(
            'id'        => $image->id,
            'uuid'      => isset($image->uuid) ? $image->uuid : null,
            'name'      => $file->basename,
            'singleSRC' => $image->path,
            'size'      => serialize($size),
            'alt'       => $imageMeta['title'],
            'imageUrl'  => $imageMeta['link'],
            'caption'   => $imageMeta['caption'],
        );

        $image = array_merge($image, $item);

        $imageObject = new FrontendTemplate('dse_image_object');
        $this->addImageToTemplate($imageObject, $image, $maxSize, $lightboxId);
        $imageObject = (object) $imageObject->getData();

        if (empty($imageObject->src)) {
            $imageObject->src = $imageObject->singleSRC;
        }

        return $imageObject;
    }

    /**
     * Creates special label for checkboxes with a colored style
     *
     * @param string $bgc   The Background Color.
     * @param string $fgc   The Foreground Color.
     * @param string $title The Label Title.
     *
     * @return string
     */
    public static function refColor($bgc, $fgc, $title)
    {
        $style = 'display:inline-block;width:100px;text-align:center;';
        return '<span style="background-color:' . $bgc . ';color:' . $fgc . ';' . $style . '">' . $title . '</span>';
    }
}
